#!/bin/bash

# Read draft title and check for duplicates. Resulting file names are created by
# removing all punctuation from the title, changing spaces to dashes, and making
# everything lowercase.
read -p 'Title: ' TITLE
if [ ! "$TITLE" ]; then
	echo "You must enter a title." 1>&2
	exit 1
fi

FILE=_drafts/$(echo $TITLE | tr -d '[:punct:]' | 
	tr '[:upper:][:blank:]' '[:lower:]\-').md
if [ -e $FILE ]; then
	echo "Duplicate draft title or resulting file name." 1>&2
	exit 1
fi

# Read (optional) excerpt.
read -p 'Excerpt: ' EXCERPT

# Read (optional) space-separated categories.
read -p 'Categories: ' CATEGORIES

# Build draft file.
echo "---" > $FILE
echo "layout: post" >> $FILE
echo "title: \"$TITLE\"" >> $FILE
[ "$EXCERPT" ] && echo "excerpt: \"$EXCERPT\"" >> $FILE
echo "categories:${CATEGORIES:+ $CATEGORIES}" >> $FILE
echo "---" >> $FILE
echo "" >> $FILE

# Edit draft.
vim $FILE

