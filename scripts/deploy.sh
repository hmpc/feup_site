#!/bin/bash

USERNAME="ee09142"
LDIR="_site"

RNET="eduroam"
RDIR="~/public_html"
RHOST="pinguim.fe.up.pt"
RPORT=22

THOST="tcpgate.fe.up.pt"
TPORT=22
LPORT=3142
TCTRL="deploy_ctrl"

HOST=$RHOST
PORT=$RPORT

# Check if default remote is reachable, otherwise set up tunnel
host $HOST &> /dev/null
if [ $? -ne 0 ]; then
	ssh -q -p$TPORT -M -S $TCTRL -fnNT -L$LPORT:$RHOST:$RPORT $USERNAME@$THOST
	if [ $? -ne 0 ]; then
		echo "Failed to set up tunnel." >&2
		exit 1
	fi

	CTRL=$TCTRL
	HOST="localhost"
	PORT=$LPORT
fi

ORIGIN=$(pwd)
cd $LDIR

# Find modified files and add to temporary timestamped archive
RHASH=$( ssh -p$PORT $USERNAME@$HOST \
	"cd $RDIR; find . -name \"*\" -type f -exec sha1sum {} +" )
LFILES=$( find . -name "*" -type f )
RFILES=$( echo "$RHASH" | awk '{print $2}' )
ARCHIVE="$( date +%s ).tar"
for file in $LFILES; do
	rhash=$( echo "$RHASH" | grep "[[:space:]]/*$file" | awk '{print $1}' )
	fhash=$( sha1sum $file | awk '{print $1}' )
	if [ "$rhash" != "$fhash" ]; then
		if [ $rhash ]; then
			echo "M $file"
		else
			echo "N $file"
		fi
		tar uf $ARCHIVE $file
	fi
done

# Find files on server with no local match
OBSOLETE=""
for file in $RFILES; do
	if [ ! -e $file ]; then
		echo "X $file"
		OBSOLETE+="$file "
	fi
done

# Copy archive to remote, extract, and clean up
if [ -e $ARCHIVE ]; then
	scp -P$PORT $ARCHIVE $USERNAME@$HOST:$RDIR
	ssh -q -p$PORT $USERNAME@$HOST "cd $RDIR; tar xf $ARCHIVE; \
		rm $ARCHIVE"
fi

# Remove files that no longer exist locally
if [ "$OBSOLETE" ]; then
	ssh -q -p$PORT $USERNAME@$HOST "cd $RDIR; rm $OBSOLETE; \
		find . -type d -empty -delete"
fi

# Clean up
rm -f $ARCHIVE
cd $ORIGIN
if [ $CTRL ]; then
	ssh -q -S $CTRL -O exit $USERNAME@$THOST 2> /dev/null
fi

