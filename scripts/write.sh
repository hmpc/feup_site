#!/bin/bash

DRAFTS=_drafts
DRAFT_EXTS=*.md
DRAFT_LIST=$(ls -1 $DRAFTS/$DRAFT_EXTS | sed -n 's|.*/\(.*\)$|\1|p')

if [ ! "$DRAFT_LIST" ]; then
	exec scripts/draft.sh
	exit $?
elif [ $(echo "$DRAFT_LIST" | wc -l) -eq 1 ]; then
	DRAFT="$DRAFT_LIST"
else
	select d in $DRAFT_LIST; do
		DRAFT=$d
		break
	done
fi

vim "$DRAFTS/$DRAFT"

