#!/bin/bash

DRAFTS=_drafts
DRAFT_EXTS=*.md
DRAFT_LIST=$(ls -1 $DRAFTS/$DRAFT_EXTS | sed -n 's|.*/\(.*\)$|\1|p')

if [ $(echo "$DRAFT_LIST" | wc -l) -eq 1 ]; then
	DRAFT="$DRAFT_LIST"
else
	select d in $DRAFT_LIST; do
		DRAFT=$d
		break
	done
fi

POST=_posts/$(date +%Y-%m-%d)-$DRAFT;
sed "s/^categories:/date: $(date +"%Y-%m-%d %T")\ncategories:/" \
	"$DRAFTS/$DRAFT" > $POST
rm "$DRAFTS/$DRAFT"

