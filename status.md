---
layout: page
title: Status updates
---

**Current status:** finished!
- - -

{% for category in site.categories %}
{% if category.first == 'status' %}
<ul class="post-list">
	{% for post in category.last %}
	<li><span>{{ post.date | date_to_string }}</span> &raquo; <a
		href="{{ site.baseurl }}/{{ post.url | remove_first:'/' }}">{{ post.title }}</a></li>
	<p>{{ post.excerpt | remove: '<p>' | remove: '</p>' }}</p>
	{% endfor %}
</ul>
{% endif %}
{% endfor %}


