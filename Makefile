SCRIPTS = scripts
LOCAL_URL = http://localhost:4000/
REMOTE_URL = http://paginas.fe.up.pt/~ee09142/

all: build deploy

build:
	@jekyll build

deploy:
	@$(SCRIPTS)/deploy.sh
	@xdg-open $(REMOTE_URL)

test:
	@jekyll serve -b '' -D & xdg-open $(LOCAL_URL) & wait $!

draft:
	@$(SCRIPTS)/draft.sh

write:
	@$(SCRIPTS)/write.sh

publish:
	@$(SCRIPTS)/publish.sh

clean:
	@rm -rf _site

.PHONY: all build deploy test draft write publish clean

