---
layout: page
title: Posts
---

{% for category in site.categories %}
{% if category.first == 'thesis' %}
<ul class="post-list">
	{% for post in category.last %}
	<li><span>{{ post.date | date_to_string }}</span> &raquo; <a
		href="{{ site.baseurl }}/{{ post.url | remove_first:'/' }}">{{ post.title }}</a></li>
	<p>{{ post.excerpt | remove: '<p>' | remove: '</p>' }}</p>
	{% endfor %}
</ul>
{% endif %}
{% endfor %}

