---
layout: page
title: "About me"
---

I'm a 22 year old ECE student at [FEUP][feup], doing my MSc thesis on
[underwater acoustic communication][thesis].  My technical interests are
embedded systems, mobile robotics, and digital communications.  I've worked on
the FEUP Autonomous Sailboat (FASt) for over four years, and I've participated
in a few other mixed hardware and software projects.  You can read the boring
details on my [CV][cv] (PDF).

[feup]: http://www.fe.up.pt/
[thesis]: {{ site.baseurl }}/posts
[cv]: {{ site.baseurl }}{{ site.files }}/CV.pdf

I'm a [worse is better][wib] perfectionist obsessed with automation.  I draw
inspiration from design for my engineering work, whether hardware or software.
I build [MPZ 22s][mpz], not [Juicy Salifs][starck].

[wib]: http://doc.cat-v.org/programming/worse_is_better
[mpz]: http://www.braunhousehold.com/Global/Product%20Images/Breakfast/Citrus%20juicers/MPZ22/MPZ22-CitrusJuicer-1.jpg
[starck]: http://upload.wikimedia.org/wikipedia/commons/5/5e/Juicy_Salif_-_78365.jpg

Outside work, I'm a generalist bookworm with a penchant for science.  My
bookshelves are lined with sci-fi, fantasy, and noir on one side, and math and
physics on the other.  I love travelling and I watch more TV series than I can
keep up with.

You can get in touch using [email][email].  If you happen to be around, swing by
I224 to grab coffee and discuss Verilog synthesis optimisations.

[email]: mailto:henrique.cabral@fe.up.pt

