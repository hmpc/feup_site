---
layout: page
title: Workplan
---

This is the most recent version of the thesis workplan.  It is updated whenever
tasks or dates change, and as tasks are completed.

| Task						| From			 | To			  | Status		  |
|:-------------------------	|:--------------:|:--------------:|:-------------:|
| **Setup**					| **2014-02-17** | **2014-03-02** | **Completed** |
| Website					| 2014-02-17	 | 2014-02-23	  | Completed	  |
| Toolchain and repository	| 2014-02-24	 | 2014-02-26	  | Completed	  |
| Initial project			| 2014-02-26	 | 2014-03-02	  | Completed	  |
| **FSK modem**				| **2014-03-03** | **2014-05-30** | **Completed** |
| Implement FSK transmitter	| 2014-03-03	 | 2014-03-26	  | Completed	  |
| Simulate FSK receiver		| 2014-03-27	 | 2014-05-23	  | Completed	  |
| Implement FSK receiver	| 2014-04-21	 | 2014-05-23	  | Completed	  |
| Test FSK modem			| 2014-04-07	 | 2014-05-30	  | Completed	  |
| **Finalisation**			| **2014-06-02** | **2014-06-27** | **Completed** |
| Evaluate FSK test results	| 2014-06-02	 | 2014-06-13	  | Completed	  |
| Clean up code and doc		| 2014-06-02	 | 2014-06-08	  | Completed	  |
| Write thesis				| 2014-06-02	 | 2014-06-27	  | Completed	  |

