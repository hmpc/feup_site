---
layout: default
title: Home
---

<h1 class="page-title">Welcome.</h1>

**Update:** the final thesis report for jury evaluation is available
[here][thesis].

[thesis]: {{ site.baseurl }}{{ site.files }}/thesis.pdf

My name is Henrique Cabral.  I am building a reconfigurable acoustic modem for
underwater communication (UWAM) for my MSc thesis, ``Acoustic modem for
underwater communication'', under [Professor José Carlos Alves][jca].  You can
read more about it [here][intro].

[intro]: {{ site.baseurl }}{% post_url 2014-02-20-what-is-this-all-about %}
[jca]: https://sigarra.up.pt/feup/pt/func_geral.formview?p_codigo=210017

If you're interested in the gory technical details, you can follow my occasional
[posts][posts] on the project architecture and implementation.  Or if you want
to see how badly behind schedule I am, you can check my [status updates][status]
and the [workplan][workplan].  Finally, you can read more about the [charming
bloke][about] doing this.

[posts]: {{ site.baseurl }}/posts
[status]: {{ site.baseurl }}/status
[workplan]: {{ site.baseurl }}/workplan
[about]: {{ site.baseurl }}/about

Thanks for visiting, and enjoy!

