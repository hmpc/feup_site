---
layout: post
title: "What is this all about?"
excerpt: "Why the problem I'm solving matters, and why you should take a look at
my work."
date: 2014-02-20 01:16:14
categories: thesis
---

Say you manage a deep-water oil rig in the Norwegian Sea (average depth
2000&nbsp;m).  You need to build and inspect facilities, for which you would presumably
use divers.  But professional divers are expensive and their activity is
strictly regulated, limiting your working schedule and maximum depth.  What can
you do about this?

In this day and age, you would use ROVs (Remotely Operated Vehicles), underwater
robots capable of performing all of the heavy lifting (literally) while being
controlled from the rig.  Traditionally, ROVs are physically linked to the
control station by a tether.  This tether, however, is a major design choke
point: it needs to have high breaking strength, carry power and data signals to
and from the ROV, and it must usually be neutrally buoyant.  Not only are they
difficult and expensive to manufacture, but tethers also limit the operational
radius of ROVs, which can't move farther from the base station than the tether
length will allow them.  So you start wondering, *isn't there a better way?*

Or maybe, while you're on it, you need a way to redundantly control the
[BOP][bop] stack (the system that failed in the Deepwater Horizon spill---see
how this stuff is important?).  What can you use that is fast, reliable, and
will still work when the main control tether fails?

[bop]: http://en.wikipedia.org/wiki/Blowout_preventer "Blowout preventer"


The solution
------------
In both cases, the answer is: *use an acoustic link!*  You may think sound is
for whales and dolphins, but in the harsh underwater environment, where radio
waves won't travel farther than a few metres and light is easily dispersed and
blocked, sound dominates wireless communication.

There are, nonetheless, numerous problems with underwater sound propagation: the
channel is highly reverberant and dispersive, with frequency-dependent
attenuation and noticeable Doppler shift and spread.  Communicating under these
conditions can be likened to transmitting information reliably over radio from
a satellite flying at LEO speed through Manhattan---if the speed of light were
two orders of magnitude lower and high frequencies died out in a range of
metres.

Despite these obstacles, underwater acoustic communication as a field has seen
remarkable improvements since its beginning in the 1970s.[^1]  Acoustic modems
are now routinely used for the cases described above.  AUVs (Autonomous
Underwater Vehicles, the next step from ROVs) usually incorporate them for
localisation and vehicle-to-surface communications.  There has also been a lot
of activity in underwater sensor networks for oceanographic research, with
sensors capable of continuously logging environmental data and relaying it
acoustically to shore, using a hybrid network composed of sensors, vehicles, and
buoys.

[^1]: I'm only considering digital communications here, not analog systems like Gertrude which have a much longer history.

These advances have been achieved largely through research into different
modulation and coding schemes.  Initial modems, and still many commercially used
systems, relied on incoherent modulations like FSK, which sidestepped
intersymbolic interference caused by large delay spreads.  Coherent systems only
became feasible for horizontal shallow-water links after advances in equaliser
structures in the 1990s, which in turn took advantage of the increased
computational power available in the form of DSP chips.


The project
----------
The great algorithmic improvements in the field, however, have left a void
in terms of usability, extensibility, and configurability.  This is the design
space we will be exploring, by producing a FPGA-based platform which will:

* permit experimentation with different modulation and diversity schemes;
* allow the designer to quickly assess online system performance;
* be parametrisable, both online and offline;
* allow configurable power consumption/performance trade-offs;
* interface with commonly used acoustic transducers.

We will implement the common FSK and PSK modulations on a [GODIL FPGA
board][godil], using Verilog HDL for the FPGA project and MathWorks Simulink for
high-level design and simulation.  The system will be field-tested in the river
and compared with existing commercial and research modems.

[godil]: http://www.oho-elektronik.de/ "OHO-Elektronik GODIL"

I will use this site to post updates on our work and related subjects, like the
development environment and philosophy, or just the latest frustrating toolchain
bug.  I hope in this way to document our creation process and bring some
attention to this fascinating frontier of telecommunications.

