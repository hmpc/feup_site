---
layout: post
title: "Week #3: 2014/03/03-09"
excerpt: "Making something run."
date: 2014-03-09 19:02:37
categories: status
---

This week was part toolchain setup and part HDL coding.  I had to iron out some
kinks in the integration between ISE and Questa, and ended up just writing some
custom simulation scripts.  I also set up some useful vim automation to include
descriptive headers in newly created project files, indicating the time of
creation and the author.

I began work on the actual FSK transmitter by creating a high-level black box
diagram of the architecture, from serial module input to analog output.  This
will be guiding the implementation, downstream to upstream in order to make
verification easier.

To start off, I set up two Xilinx IP Cores: a DCM (Digital Clock Manager) to
generate a 98.304&nbsp;MHz clock from the on-board crystal, and a DDS (Direct
Digital Synthesiser) to produce the modulated output, which will be converted to
an analog signal for the transducer.  The DDS core has been verified for
frequencies between 15 and 30&nbsp;kHz, and it produces a sufficiently pure
spectrum (as checked in MATLAB).

Apart from these modules, I implemented a simple register-based configuration
interface for the transmitter, which uses a BRAM to store configuration values.
This was not verified, since it's a rather simple instance of memory inference.
Finally, I began work on the modulator, which will be responsible for converting
symbols to frequencies, and possibly performing frequency hopping.

